# 08 - Requisições e Network

## `fetch`

O React Native fornece a API de busca para suas necessidades de rede. A busca parecerá familiar se você já usou XMLHttpRequest ou outras APIs de rede antes. Você pode consultar o [guia do MDN](https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API) sobre o uso de busca para obter informações adicionais.

**GET**
``` javascript
fetch('https://mywebsite.com/mydata.json');
```

**POST**
``` javascript
fetch('https://mywebsite.com/endpoint/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    firstParam: 'yourValue',
    secondParam: 'yourOtherValue',
  }),
});
```

## Outros métodos

A API `XMLHttpRequest` está incorporada no React Native. Isso significa que você pode usar bibliotecas de terceiros, como `frisbee` ou `axios`.

#### Doc
- https://facebook.github.io/react-native/docs/network
 
# Desafio

O desafio poderá ter duas escolhas, tema livre ou o especificado por nós, para o tema livre embora você possa fazer o que preferir os mesmos tópicos do especificado serão cobrados.

Existem ferramentas para testarem as requisições antes de colocarem no código, como [Insomnia](https://insomnia.rest/) e [Postman](https://www.getpostman.com/), elas serão grandes aliadas nos testes de vocês. 

## Tema livre

Use qualquer uma das APIs disponiveis na URL abaixo, faça **PELO MENOS UMA** exibição de lista (FlatList ou SectionList). Tambem faça **PELO MENOS DUAS** requisições. (DICA: Escolha uma que não possua autenticação)

https://github.com/toddmotto/public-apis

## Especificado

Utilize a API da FIPE abaixo para pesquisar por Marca, Modelo e Ano um carro e exibir os dados dele. 

https://deividfortuna.github.io/fipe/

- Faça pelo menos 3 caixas de seleção para cada pesquisa
- O resultado da busca de Marca deve ser colocada na primeira caixa de seleção para a escolha do usuário
- A pesquisa por Modelo deve ficar DESABILITADA enquanto a pesquisa de Marca não for escolhida
- A pesquisa por Ano deve ficar DESABILITADA enquanto a pesquisa de Modelo não for escolhida

### Inicialização:

- De um Fork no repositório: `https://gitlab.com/acct.fateclab/turma-1-sem-2019/08-requisicoes-e-network`;
- Agora você deve clonar o repositório localmente, há um botão azul "Clone" no seu repositório do GitLab, clique nele e use a URL com HTTPS: `git clone url-copiada`;
- Agora dentro da pasta clonada, clique com o botão direito do mouse e clique em "Git Bash Here" para entrar na pasta pelo terminal;
- Para iniciar o servidor de desenvolvimento não se esqueça de usar `npm run start` e escanear o QRCode no aplicativo - Expo para que o App continue sendo buildado sempre que atualizar os arquivos.

### Entrega:

- Faça commits para cada forma diferente de exibição do texto;
- Assim que terminar dê `git push origin seu-nome/requisicoes-e-network`, crie um Merge Request na interface do GitLab, acesse o menu "Merge Requests" e crie um, configure o "Target Branch" para o repositório original para que seu App seja avaliado e revisado e para que possamos te dar um feedback;
- O nome do Merge Request deve ser o seu nome completo.